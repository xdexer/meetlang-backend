from django.db import models


class PersonalityTag(models.Model):
    name = models.CharField(max_length=50)

    @classmethod
    def get_default_object(cls):
        tag, _ = cls.objects.get_or_create(
            name="Unset"
        )
        return tag.pk

    def __str__(self) -> str:
        return f"Tag: {self.name}"
