from django.db import models

from auth_module.models.meetlang_user import MeetlangUser


class MatchRequest(models.Model):
    request_from = models.ForeignKey(MeetlangUser, on_delete=models.CASCADE, related_name='request_from')
    request_to = models.ForeignKey(MeetlangUser, on_delete=models.CASCADE, related_name='request_to')
    is_obsolete = models.BooleanField(default=False)
