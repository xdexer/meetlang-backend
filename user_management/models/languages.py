from django.db import models


class Language(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=3)
    common = models.BooleanField(default=False)
    flag = models.CharField(max_length=50, default='')

    @classmethod
    def get_default_object(cls):
        language, _ = cls.objects.get_or_create(
            name="Unset",
            code="N/A",
            common=True,
            flag='')
        return language.pk

    def __str__(self) -> str:
        return f"Language: {self.name}, {self.code}"
