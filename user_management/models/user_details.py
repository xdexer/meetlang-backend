from django.db import models
from PIL import Image

from auth_module.models.meetlang_user import MeetlangUser
from user_management.models.languages import Language
from user_management.models.user_tags import PersonalityTag
# Create your models here.


class UserDetails(models.Model):
    first_name = models.CharField(max_length=30)
    is_active = models.BooleanField(default=False)
    user_description = models.TextField()
    age = models.IntegerField()
    sex = models.BooleanField()
    native_language = models.ForeignKey(Language, default=Language.get_default_object,
                                        on_delete=models.PROTECT, related_name='native_languages')
    language_to_learn = models.ForeignKey(Language, default=Language.get_default_object,
                                          on_delete=models.PROTECT, related_name='languages_to_learn')
    tags = models.ManyToManyField(PersonalityTag, default=PersonalityTag.get_default_object)
    avatar = models.ImageField(upload_to='avatars', blank=True, null=True)
    user = models.OneToOneField(MeetlangUser, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        super(UserDetails, self).save(*args, **kwargs)
        if self.avatar.name:
            img = Image.open(self.avatar.path)
            if img.height > 300 or img.width > 300:
                output_size = (300, 300)
                img.thumbnail(output_size)
                img.save(self.avatar.path)

    def __str__(self) -> str:
        return f"{self.first_name} : {self.age} : {self.is_active}"
