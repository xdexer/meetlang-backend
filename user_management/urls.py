"""meetlang_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from rest_framework.routers import DefaultRouter

from user_management.views.languages import LanguageViewSet
from user_management.views.user_details import UserDetailsViewSet
from user_management.views.user_tags import PersonalityTagViewSet

from .api.views import MatchManagmentViewset

router = DefaultRouter()
router.register(r'user_management', UserDetailsViewSet)
router.register(r'language', LanguageViewSet)
router.register(r'personality_tag', PersonalityTagViewSet)
router.register('match', MatchManagmentViewset, basename='match')


urlpatterns = [
    path('', include(router.urls)),
]

urlpatterns += router.urls
