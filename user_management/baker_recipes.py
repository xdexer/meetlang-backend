from model_bakery.recipe import Recipe, foreign_key, seq, related
from itertools import cycle

from auth_module.baker_recipes import user
from .models import UserDetails, MatchRequest, Language, PersonalityTag

user_details = Recipe(
    UserDetails,
    first_name=seq('Name '),
    age=cycle(range(18, 24)),
    sex=cycle([True, False]),
    user=foreign_key(user),
)

match_request = Recipe(
    MatchRequest,
    request_from=foreign_key(user),
    request_to=foreign_key(user),
    is_obsolete=False
)

language = Recipe(
    Language,
    name=seq('Name '),
    common=True
)

personality_tag = Recipe(
    PersonalityTag,
    name=seq('Name ')
)

user_details_with_tags = user_details.extend(
    tags=related(personality_tag, personality_tag)
)
