from rest_framework import serializers
from django.db.models import Q

from auth_module.models.meetlang_user import MeetlangUser
from user_management.models import MatchRequest
from chat_management.models import ChatRoom


class AcceptMatchSerializer(serializers.Serializer):
    other_user = serializers.PrimaryKeyRelatedField(queryset=MeetlangUser.objects.all())

    def validate(self, attrs):
        user = self.context.get('request').user
        other_user = attrs.get('other_user')
        if not other_user:
            raise serializers.ValidationError(f'User doesnt exist')

        if user == other_user:
            raise serializers.ValidationError('Cant create match request with yourself')

        if MatchRequest.objects.filter(request_from=user, request_to=other_user).exists():
            raise serializers.ValidationError(f'MatchRequest for {user} - {other_user} already exists')

        if ChatRoom.objects.filter(Q(user_1=user, user_2=other_user) | Q(user_1=other_user, user_2=user)).exists():
            raise serializers.ValidationError(f'ChatRoom for {user} - {other_user} already created')

        return super().validate(attrs)

    def create(self, validated_data):
        user = self.context.get('request').user
        other = validated_data.get('other_user')

        if MatchRequest.objects.filter(request_from=other, request_to=user).exists():
            request = MatchRequest.objects.get(request_from=other, request_to=user)
            request.is_obsolete = True
            request.save()
            ChatRoom.objects.create(user_1=user, user_2=other)
            return other

        MatchRequest.objects.create(request_from=user, request_to=other)
        return other


class RefuseMatchSerializer(serializers.Serializer):
    other_user = serializers.PrimaryKeyRelatedField(queryset=MeetlangUser.objects.all())

    def validate(self, attrs):
        user = self.context.get('request').user
        other_user = attrs.get('other_user')

        if not other_user:
            raise serializers.ValidationError(f'User doesnt exist')

        if ChatRoom.objects.filter(Q(user_1=user, user_2=other_user) | Q(user_1=other_user, user_2=user)).exists():
            raise serializers.ValidationError(f'Cannot reject MatchRequest for already created ChatRoom')

        return super().validate(attrs)

    def create(self, validated_data):
        user = self.context.get('request').user
        other = validated_data.get('other_user')
        try:
            request = MatchRequest.objects.get(request_from=other, request_to=user)
            request.is_obsolete = True
            request.save()
        except:
            pass

        return other
