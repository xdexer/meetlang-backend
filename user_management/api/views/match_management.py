from rest_framework import viewsets, status

from rest_framework.decorators import action
from rest_framework.response import Response

from auth_module.models.meetlang_user import MeetlangUser
from ..serializers.match import AcceptMatchSerializer, RefuseMatchSerializer


class MatchManagmentViewset(viewsets.GenericViewSet):
    queryset = MeetlangUser.objects.all()

    @action(detail=False, methods=['post'])
    def accept(self, request):
        serializer = AcceptMatchSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['post'])
    def refuse(self, request):
        serializer = RefuseMatchSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_200_OK)
