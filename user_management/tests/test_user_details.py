import pytest
from model_bakery.baker import make_recipe

from user_management.models import UserDetails, PersonalityTag


def make_url(val: str) -> str:
    return f'/backend/user/user_management/{val}'


@pytest.mark.django_db
def test_create_user_details(api_client):
    make_recipe('user_management.user_details', user__email='user1@gmail.com', user__id=1)
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)
    tag1 = PersonalityTag.objects.create(name="Gaming")
    tag2 = PersonalityTag.objects.create(name="Journeys")
    api_client.post(make_url(''), {
        'first_name': 'Testowy',
        'is_active': 'True',
        'user_description': 'sfssgffdggsf',
        'age': '2137',
        'sex': 'True',
        'user': f'{new_user.pk}',
        "tags": [tag1.id, tag2.id]
    })
    assert UserDetails.objects.all().count() == 2


@pytest.mark.django_db
def test_get_user_details_with_personality_tags(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)
    tag1 = PersonalityTag.objects.create(name="Gaming")
    tag2 = PersonalityTag.objects.create(name="Journeys")
    make_recipe('user_management.user_details_with_tags',
                first_name='Testowy',
                is_active=True,
                user_description='sfssgffdggsf',
                age=2137,
                sex=True,
                tags=[tag1, tag2],
                user=new_user)

    get_user_details = api_client.get(make_url(f'{new_user.id}/'))
    assert get_user_details.json()['tags'] == [{'id': 1, 'name': 'Gaming'}, {'id': 2, 'name': 'Journeys'}]


@pytest.mark.django_db
def test_get_user_details(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    make_recipe('user_management.user_details',
                first_name='Testowy',
                is_active=True,
                user_description='sfssgffdggsf',
                age=2137,
                sex=True,
                user=new_user)

    get_user_details = api_client.get(make_url(f'{new_user.id}/'))
    assert get_user_details.json()['age'] == 2137
    assert get_user_details.json()['first_name'] == 'Testowy'


@pytest.mark.django_db
def test_update_user_details(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    make_recipe('user_management.user_details',
                first_name='Testowy',
                is_active=True,
                user_description='sfssgffdggsf',
                age=2137,
                sex=True,
                user=new_user)

    get_user_details = api_client.patch(make_url(f'{new_user.id}/'), {'age': '2138', 'first_name': 'Jan Pawel'})
    assert get_user_details.json()['age'] == 2138
    assert get_user_details.json()['first_name'] == 'Jan Pawel'


@pytest.mark.django_db
def test_update_user_details_tags(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    make_recipe('user_management.user_details',
                first_name='Testowy',
                is_active=True,
                user_description='sfssgffdggsf',
                age=2137,
                sex=True,
                user=new_user)

    tag1 = PersonalityTag.objects.create(name="Gaming")
    tag2 = PersonalityTag.objects.create(name="Journeys")
    get_user_details = api_client.patch(make_url(f'{new_user.id}/'),
                                        data={"tags": [tag1.id, tag2.id]}, format='json')
    assert get_user_details.json()['tags'] == [tag1.id, tag2.id]
    assert get_user_details.status_code == 200

    tag3 = PersonalityTag.objects.create(name="Party")
    tag4 = PersonalityTag.objects.create(name="Books")
    get_user_details = api_client.patch(make_url(f'{new_user.id}/'),
                                        data={"tags": [tag3.id, tag4.id]}, format='json')
    assert get_user_details.json()['tags'] == [tag3.id, tag4.id]
    assert get_user_details.status_code == 200


@pytest.mark.django_db
def test_delete_user_details(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    make_recipe('user_management.user_details',
                first_name='Testowy',
                is_active=True,
                user_description='sfssgffdggsf',
                age=2137,
                sex=True,
                user=new_user)

    delete_details = api_client.delete(make_url(f'{new_user.id}/'))
    assert delete_details.status_code == 204
    assert UserDetails.objects.all().count() == 0
