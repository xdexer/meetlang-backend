import pytest
from model_bakery.baker import make_recipe

from user_management.models import PersonalityTag


def make_url(val: str) -> str:
    return f'/backend/user/personality_tag/{val}'


@pytest.mark.django_db
def test_create_personality_tag(api_client):
    make_recipe('user_management.user_details', user__email='user1@gmail.com', user__id=1)
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    api_client.post(make_url(''), {
        'name': 'Testowy'
    })

    assert PersonalityTag.objects.all().count() == 1


@pytest.mark.django_db
def test_get_personality_tag(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    tag = make_recipe('user_management.personality_tag',
                      name='Testowy'
                      )

    get_personality_tag = api_client.get(make_url(f'{tag.id}/'))
    assert get_personality_tag.json()['name'] == 'Testowy'


@pytest.mark.django_db
def test_update_personality_tag(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    tag = make_recipe('user_management.personality_tag',
                      name='Testowy'
                      )

    get_personality_tag = api_client.patch(make_url(f'{tag.id}/'), {'name': 'Testerski'})
    assert get_personality_tag.json()['name'] == 'Testerski'


@pytest.mark.django_db
def test_delete_personality_tag(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    tag = make_recipe('user_management.personality_tag',
                      name='Testowy'
                      )

    delete_details = api_client.delete(make_url(f'{tag.id}/'))
    assert delete_details.status_code == 204
    assert PersonalityTag.objects.all().count() == 0
