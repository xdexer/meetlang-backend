import pytest
from model_bakery.baker import make_recipe

from chat_management.models import ChatRoom
from user_management.models import MatchRequest


def make_url(accept=True) -> str:
    return '/backend/user/match/accept/' if accept else '/backend/user/match/refuse/'


@pytest.mark.django_db
def test_create_match(api_client):
    user = make_recipe('user_management.user_details', user__email='user1@gmail.com', user__id=0)
    user_2 = make_recipe('user_management.user_details', user__email='user2@gmail.com', user__id=1)

    api_client.force_authenticate(user.user)
    api_client.post(make_url(), {'other_user': user_2.user.id})

    assert MatchRequest.objects.filter(request_from=user.user, request_to=user_2.user).exists() is True


@pytest.mark.django_db
def test_create_chat_room(api_client):
    user = make_recipe('user_management.user_details', user__email='user1@gmail.com', user__id=0)
    user_2 = make_recipe('user_management.user_details', user__email='user2@gmail.com', user__id=1)
    make_recipe('user_management.match_request', request_from=user.user, request_to=user_2.user)

    api_client.force_authenticate(user_2.user)
    api_client.post(make_url(), {'other_user': user.user.id})

    assert ChatRoom.objects.filter(user_1=user_2.user, user_2=user.user).exists() is True


@pytest.mark.django_db
def test_refuse_match_request(api_client):
    user = make_recipe('user_management.user_details', user__email='user1@gmail.com', user__id=0)
    user_2 = make_recipe('user_management.user_details', user__email='user2@gmail.com', user__id=1)
    request = make_recipe('user_management.match_request', request_from=user.user, request_to=user_2.user)

    api_client.force_authenticate(user_2.user)
    api_client.post(make_url(False), {'other_user': user.user.id})

    request.refresh_from_db()
    assert ChatRoom.objects.filter(user_1=user_2.user, user_2=user.user).exists() is False
    assert request.is_obsolete is True
