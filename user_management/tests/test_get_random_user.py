import pytest
from model_bakery.baker import make_recipe

from user_management.models import Language


def make_url() -> str:
    return '/backend/user/user_management/get_random_user/'


@pytest.mark.django_db
def test_get_random_user(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)
    make_recipe('user_management.user_details',
                first_name='Testowy',
                is_active=True,
                user_description='sfssgffdggsf',
                age=2137,
                sex=True,
                user=new_user)

    other_user1 = make_recipe('auth_module.user')
    user_details = make_recipe('user_management.user_details',
                               first_name='Testerski',
                               is_active=True,
                               user_description='fdgsdfsd',
                               age=12,
                               sex=False,
                               user=other_user1)

    get_user_details = api_client.get(make_url())
    assert get_user_details.json()['id'] == user_details.pk


@pytest.mark.django_db
def test_get_random_user_when_there_is_no(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)
    make_recipe('user_management.user_details',
                first_name='Testowy',
                is_active=True,
                user_description='sfssgffdggsf',
                age=2137,
                sex=True,
                user=new_user)

    get_user_details = api_client.get(make_url())
    assert get_user_details.data == "No matching users found"
    assert get_user_details.status_code == 204


@pytest.mark.django_db
def test_get_random_user_lang_filtering(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)
    lan1 = Language.objects.create(**{'name': 'English', 'code': 'EN', 'common': True, 'flag': '\U0001F1EC\U0001F1E7'})
    lan2 = Language.objects.create(**{'name': 'Polish', 'code': 'PL', 'common': True, 'flag': '\U0001F1F5\U0001F1F1'})
    make_recipe('user_management.user_details',
                first_name='Testowy',
                is_active=True,
                user_description='sfssgffdggsf',
                age=2137,
                sex=True,
                user=new_user,
                native_language=lan1,
                language_to_learn=lan2)

    other_user1 = make_recipe('auth_module.user')
    make_recipe('user_management.user_details',
                first_name='Testerski',
                is_active=True,
                user_description='fdgsdfsd',
                age=12,
                sex=False,
                user=other_user1,
                native_language=lan1,
                language_to_learn=lan2)

    other_user2 = make_recipe('auth_module.user')
    user_details = make_recipe('user_management.user_details',
                               first_name='Polski',
                               is_active=True,
                               user_description='dsfsfgfa',
                               age=54,
                               sex=True,
                               user=other_user2,
                               native_language=lan2,
                               language_to_learn=lan1)

    get_user_details = api_client.get(make_url())
    assert get_user_details.json()['id'] == user_details.pk


@pytest.mark.django_db
def test_get_random_user_lang_filtering_when_already_has_chat(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)
    lan1 = Language.objects.create(**{'name': 'English', 'code': 'EN', 'common': True, 'flag': '\U0001F1EC\U0001F1E7'})
    lan2 = Language.objects.create(**{'name': 'Polish', 'code': 'PL', 'common': True, 'flag': '\U0001F1F5\U0001F1F1'})
    make_recipe('user_management.user_details',
                first_name='Testowy',
                is_active=True,
                user_description='sfssgffdggsf',
                age=2137,
                sex=True,
                user=new_user,
                native_language=lan1,
                language_to_learn=lan2)

    other_user1 = make_recipe('auth_module.user')
    make_recipe('user_management.user_details',
                first_name='Testerski',
                is_active=True,
                user_description='fdgsdfsd',
                age=12,
                sex=False,
                user=other_user1,
                native_language=lan1,
                language_to_learn=lan2)

    other_user2 = make_recipe('auth_module.user')
    make_recipe('user_management.match_request', request_from=other_user2,
                request_to=new_user, is_obsolete=True)
    make_recipe('user_management.user_details',
                first_name='Polski',
                is_active=True,
                user_description='dsfsfgfa',
                age=54,
                sex=True,
                user=other_user2,
                native_language=lan2,
                language_to_learn=lan1)

    get_user_details = api_client.get(make_url())
    assert get_user_details.data == "No matching users found"
    assert get_user_details.status_code == 204


@pytest.mark.django_db
def test_get_random_user_lang_filtering_when_match_request_happened(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)
    lan1 = Language.objects.create(**{'name': 'English', 'code': 'EN', 'common': True, 'flag': '\U0001F1EC\U0001F1E7'})
    lan2 = Language.objects.create(**{'name': 'Polish', 'code': 'PL', 'common': True, 'flag': '\U0001F1F5\U0001F1F1'})
    make_recipe('user_management.user_details',
                first_name='Testowy',
                is_active=True,
                user_description='sfssgffdggsf',
                age=2137,
                sex=True,
                user=new_user,
                native_language=lan1,
                language_to_learn=lan2)

    other_user1 = make_recipe('auth_module.user')
    make_recipe('user_management.user_details',
                first_name='Testerski',
                is_active=True,
                user_description='fdgsdfsd',
                age=12,
                sex=False,
                user=other_user1,
                native_language=lan1,
                language_to_learn=lan2)

    other_user2 = make_recipe('auth_module.user')
    make_recipe('user_management.match_request', request_from=other_user2,
                request_to=new_user, is_obsolete=False)
    user_details = make_recipe('user_management.user_details',
                               first_name='Polski',
                               is_active=True,
                               user_description='dsfsfgfa',
                               age=54,
                               sex=True,
                               user=other_user2,
                               native_language=lan2,
                               language_to_learn=lan1)

    get_user_details = api_client.get(make_url())
    assert get_user_details.json()['id'] == user_details.pk
