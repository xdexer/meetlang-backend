import pytest
from model_bakery.baker import make_recipe

from user_management.models import Language


def make_url(val: str) -> str:
    return f'/backend/user/language/{val}'


@pytest.mark.django_db
def test_create_language(api_client):
    make_recipe('user_management.user_details', user__email='user1@gmail.com', user__id=1)
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    api_client.post(make_url(''), {
        'name': 'Testowy',
        'code': 'TT',
        'common': True,
        'flag': ''
    })

    assert Language.objects.all().count() == 2


@pytest.mark.django_db
def test_get_language(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    lan = make_recipe('user_management.language',
                      name='Testowy',
                      code='TT',
                      common=True,
                      flag=''
                      )

    get_language = api_client.get(make_url(f'{lan.id}/'))
    assert get_language.json()['name'] == 'Testowy'
    assert get_language.json()['code'] == 'TT'


@pytest.mark.django_db
def test_update_language(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    lan = make_recipe('user_management.language',
                      name='Testowy',
                      code='TT',
                      common=True,
                      flag=''
                      )

    get_language = api_client.patch(make_url(f'{lan.id}/'), {'name': 'Testerski', 'code': 'TS'})
    assert get_language.json()['name'] == 'Testerski'
    assert get_language.json()['code'] == 'TS'


@pytest.mark.django_db
def test_delete_language(api_client):
    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    lan = make_recipe('user_management.language',
                      name='Testowy',
                      code='TT',
                      common=True,
                      flag=''
                      )

    delete_details = api_client.delete(make_url(f'{lan.id}/'))
    assert delete_details.status_code == 204
    assert Language.objects.all().count() == 0
