from rest_framework import serializers

from user_management.models.user_tags import PersonalityTag


class PersonalityTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonalityTag
        fields = '__all__'
