from rest_framework import serializers

from user_management.models.user_details import UserDetails
from .languages import LanguageSerializer
from .user_tags import PersonalityTagSerializer


class UserDetailsReadSerializer(serializers.ModelSerializer):
    native_language = LanguageSerializer()
    language_to_learn = LanguageSerializer()
    tags = PersonalityTagSerializer(many=True)

    class Meta:
        model = UserDetails
        fields = '__all__'
