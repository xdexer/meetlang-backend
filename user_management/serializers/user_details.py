from rest_framework import serializers

from user_management.models.user_details import UserDetails


class UserDetailsSerializer(serializers.ModelSerializer):
    avatar = serializers.ImageField(allow_null=True, required=False)

    class Meta:
        model = UserDetails
        fields = '__all__'
