from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated

from user_management.models.user_tags import PersonalityTag
from user_management.serializers.user_tags import PersonalityTagSerializer


class PersonalityTagViewSet(viewsets.ModelViewSet):
    queryset = PersonalityTag.objects.all()
    serializer_class = PersonalityTagSerializer
    permission_classes = [IsAuthenticated]
