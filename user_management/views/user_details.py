import random

from django.db.models import OuterRef, Subquery
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from user_management.models import MatchRequest
from user_management.models.user_details import UserDetails
from user_management.serializers.user_details import UserDetailsSerializer
from user_management.serializers.user_details_read import \
    UserDetailsReadSerializer


class UserDetailsViewSet(viewsets.ModelViewSet):
    queryset = UserDetails.objects.all()
    serializer_class = UserDetailsSerializer
    lookup_field = 'user'
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, *args, **kwargs):
        self.serializer_class = UserDetailsReadSerializer
        return super().retrieve(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        self.serializer_class = UserDetailsReadSerializer
        return super().list(request, *args, **kwargs)

    @action(detail=False, methods=['get'])
    def get_random_user(self, request):
        current_user = request.user.pk
        current_user_details = UserDetails.objects.get(user=current_user)

        satisfying_users = UserDetails.objects.exclude(user=current_user) \
            .exclude(user__in=Subquery(MatchRequest.objects.filter(
                request_from=OuterRef('user'), request_to=current_user, is_obsolete=True)
                .values('request_from'))) \
            .exclude(user__in=Subquery(MatchRequest.objects.filter(
                request_to=OuterRef('user'), request_from=current_user)
                .values('request_to'))) \
            .filter(native_language=current_user_details.language_to_learn)

        if satisfying_users:
            queryset = random.choice(list(satisfying_users))
            serializer = UserDetailsReadSerializer(queryset)
            return Response(serializer.data)

        return Response("No matching users found", status=status.HTTP_204_NO_CONTENT)

    @action(detail=False, methods=['get'])
    def get_current_user_id(self, request):
        return Response({'user': request.user.id})
