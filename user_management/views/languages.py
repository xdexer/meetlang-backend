from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from user_management.models.languages import Language
from user_management.serializers.languages import LanguageSerializer


class LanguageViewSet(viewsets.ModelViewSet):
    queryset = Language.objects.all()
    serializer_class = LanguageSerializer
    permission_classes = [IsAuthenticated]
