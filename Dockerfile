FROM python:3.11
ENV PYTHONUNBUFFERED 1
WORKDIR /usr/src/app
COPY . .
RUN pip install -r requirements.txt
RUN apt update
RUN apt install -y sqlite3
RUN apt-get -y install postgresql
EXPOSE 8000
CMD python manage.py runserver 0.0.0.0:8000
