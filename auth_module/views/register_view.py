from rest_framework import generics
from rest_framework.permissions import AllowAny

from auth_module.models.meetlang_user import MeetlangUser
from auth_module.serializers.register_serializer import RegisterUserSerializer


class RegisterView(generics.CreateAPIView):
    queryset = MeetlangUser.objects.all()
    permission_classes = (AllowAny, )
    serializer_class = RegisterUserSerializer
