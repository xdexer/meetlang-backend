from rest_framework.permissions import AllowAny

from auth_module.serializers.token_serializer import \
    UserTokenObtainPairSerializer
from auth_module.views.http_cookie_views import CookieTokenObtainPairView

# Create your views here.


class UserObtainTokenPairView(CookieTokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = UserTokenObtainPairSerializer
