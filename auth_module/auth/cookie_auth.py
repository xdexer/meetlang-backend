from django.conf import settings
from rest_framework_simplejwt.authentication import JWTAuthentication


class CookieHandlerJWTAuthentication(JWTAuthentication):
    def authenticate(self, request):
        access_token = request.COOKIES.get('Authorization')

        if access_token:
            request.META['HTTP_AUTHORIZATION'] = access_token

        return super().authenticate(request)
