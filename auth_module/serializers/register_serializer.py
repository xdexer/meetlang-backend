from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from auth_module.models.meetlang_user import MeetlangUser


class RegisterUserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=MeetlangUser.objects.all())]
    )
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    confirm_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = MeetlangUser
        fields = ('email', 'password', 'confirm_password')

    def validate(self, attrs):
        if attrs['password'] != attrs['confirm_password']:
            raise serializers.ValidationError({'password': 'Password fields didn\'t match.'})

        return attrs

    def create(self, validated_data):
        meetlang_user = MeetlangUser.objects.create(
            email=validated_data['email'],
        )
        meetlang_user.set_password(validated_data['password'])
        meetlang_user.save()

        return meetlang_user
