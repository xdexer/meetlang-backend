import random
import string
from typing import Any, Optional

from django.core.management.base import BaseCommand
from django.utils import timezone

from auth_module.models.meetlang_user import MeetlangUser
from chat_management.models.chat_models import ChatMessage, ChatRoom
from user_management.models.languages import Language
from user_management.models.user_details import UserDetails
from user_management.models.user_tags import PersonalityTag
from user_management.models.match_request import MatchRequest

from .languages import languages
from .tags import tags

TESTING_PASSWORD = 'testowy123'


def random_str(char_num: int) -> str:
    return ''.join(random.choice(string.ascii_letters) for _ in range(char_num))


class Command(BaseCommand):
    def generate_auth_user(self, num: int) -> MeetlangUser:
        new_user: MeetlangUser = MeetlangUser.objects.create(
            email=f'testowy{num}@gmail.com',
        )
        new_user.set_password(TESTING_PASSWORD)
        new_user.save()
        return new_user

    def generate_user_details(self, auth_user: MeetlangUser) -> UserDetails:
        new_details: UserDetails = UserDetails.objects.create(
            first_name=random_str(10),
            is_active=False,
            user_description=random_str(40),
            age=random.randint(18, 40),
            sex=random.choice((True, False)),
            native_language=Language.objects.get(
                **{'name': 'Polish', 'code': 'PL', 'common': True, 'flag': '\U0001F1F5\U0001F1F1'}),
            language_to_learn=Language.objects.get(
                **{'name': 'English', 'code': 'EN', 'common': True, 'flag': '\U0001F1EC\U0001F1E7'}),
            user=auth_user
        )
        new_details.tags.add(PersonalityTag.objects.get(name='Gaming'))
        new_details.save()
        return new_details

    def generate_chat_rooms_for_users(self, user_array: list[MeetlangUser]) -> None:
        # Generate Chat Rooms between some users and obsoleted MatchRequests
        for i in range(5):
            chat_room: ChatRoom = ChatRoom.objects.create(
                name=f"TestRoom{i}",
                user_1=user_array[i],
                user_2=user_array[2*i+1]
            )
            MatchRequest.objects.create(
                request_from=user_array[i],
                request_to=user_array[2*i+1],
                is_obsolete=True
            )
            # Generate Chat Messages for every Chat Room
            for j in range(1, 10):
                ChatMessage.objects.create(
                    user=user_array[i],
                    message=f"This User{j}",
                    chat=chat_room,
                    timestamp=timezone.now()
                )
                ChatMessage.objects.create(
                    user=user_array[2*i+1],
                    message=f"Other User{j}",
                    chat=chat_room,
                    timestamp=timezone.now()
                )

    def generate_languages(self) -> None:
        for lang in languages:
            l = Language.objects.get_or_create(**lang)
            print(f'Created language: {l}')

    def generate_personality_tags(self) -> None:
        for tag in tags:
            t = PersonalityTag.objects.get_or_create(name=tag)
            print(f'Created PersonalityTag: {t}')

    def handle(self, *args: Any, **options: Any) -> Optional[str]:
        user_array = []
        self.generate_languages()
        self.generate_personality_tags()
        for num in range(10):
            user: MeetlangUser = self.generate_auth_user(num)
            user_array.append(user)
            user_details: UserDetails = self.generate_user_details(user)
            print(f'Generated {user} with details {user_details}')
        self.generate_chat_rooms_for_users(user_array)
