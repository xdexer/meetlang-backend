from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from auth_module.managers.meetlang_user_manager import MeetlangUserManager

# Create your models here.


class MeetlangUser(AbstractUser):
    email = models.EmailField(_('email address'), unique=True)

    username = None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = MeetlangUserManager()

    def __str__(self) -> str:
        return f"User: {self.email}"
