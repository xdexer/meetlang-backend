from django.contrib import admin

from chat_management.models.chat_models import ChatMessage, ChatRoom

# Register your models here.

admin.site.register(ChatRoom)
admin.site.register(ChatMessage)
