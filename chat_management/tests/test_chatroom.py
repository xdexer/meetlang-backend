import pytest
from model_bakery.baker import make_recipe

from chat_management.models import ChatRoom


def make_url(val: str) -> str:
    return f'/backend/chat/chatroom/{val}'


@pytest.mark.django_db
def test_create_chat_room(api_client):
    user_1 = make_recipe('auth_module.user')
    user_2 = make_recipe('auth_module.user')
    make_recipe('chat_management.chat_room', name='TestRoom', user_1=user_1, user_2=user_2)

    new_user = make_recipe('auth_module.user')
    api_client.force_authenticate(new_user)

    user_3 = make_recipe('auth_module.user')
    user_4 = make_recipe('auth_module.user')
    api_client.post(make_url(''), {
        'name': 'TestRoom2',
        'user_1': f'{user_3.pk}',
        'user_2': f'{user_4.pk}'
    })

    assert ChatRoom.objects.all().count() == 2


@pytest.mark.django_db
def test_get_chat_room(api_client):
    user_3 = make_recipe('auth_module.user')
    user_4 = make_recipe('auth_module.user')

    chat_room = make_recipe('chat_management.chat_room',
                            name='TestRoom2',
                            user_1=user_3,
                            user_2=user_4)
    api_client.force_authenticate(user_3)
    get_chat_room = api_client.get(make_url(f'{chat_room.pk}/'))
    assert get_chat_room.json()['name'] == 'TestRoom2'
    assert get_chat_room.json()['user_1'] == user_3.pk


@pytest.mark.django_db
def test_list_chat_rooms(api_client):
    user_3 = make_recipe('auth_module.user')
    user_4 = make_recipe('auth_module.user')
    user_5 = make_recipe('auth_module.user')

    make_recipe('chat_management.chat_room',
                name='TestRoom2',
                user_1=user_3,
                user_2=user_4)
    make_recipe('chat_management.chat_room',
                name='TestRoom3',
                user_1=user_3,
                user_2=user_5)
    make_recipe('chat_management.chat_room',
                name='TestRoom3',
                user_1=user_4,
                user_2=user_5)

    api_client.force_authenticate(user_3)
    get_chat_room = api_client.get(make_url(''))
    print(get_chat_room.json())
    assert len(list(get_chat_room.json())) == 2


@pytest.mark.django_db
def test_update_chat_room(api_client):
    user_3 = make_recipe('auth_module.user')
    user_4 = make_recipe('auth_module.user')

    chat_room = make_recipe('chat_management.chat_room',
                            name='TestRoom2',
                            user_1=user_3,
                            user_2=user_4)
    api_client.force_authenticate(user_4)
    get_chat_room = api_client.patch(make_url(f'{chat_room.pk}/'), {'name': 'TestRoomNew'})
    assert get_chat_room.json()['name'] == 'TestRoomNew'


@pytest.mark.django_db
def test_delete_chat_room(api_client):
    user_3 = make_recipe('auth_module.user')
    user_4 = make_recipe('auth_module.user')

    chat_room = make_recipe('chat_management.chat_room',
                            name='TestRoom2',
                            user_1=user_3,
                            user_2=user_4)
    api_client.force_authenticate(user_3)
    delete_chat_room = api_client.delete(make_url(f'{chat_room.pk}/'))
    assert delete_chat_room.status_code == 204
    assert ChatRoom.objects.all().count() == 0


@pytest.mark.django_db
def test_get_not_your_chat_room(api_client):
    user_1 = make_recipe('auth_module.user')

    user_3 = make_recipe('auth_module.user')
    user_4 = make_recipe('auth_module.user')

    chat_room = make_recipe('chat_management.chat_room',
                            name='TestRoom2',
                            user_1=user_3,
                            user_2=user_4)
    api_client.force_authenticate(user_1)
    get_chat_room = api_client.get(make_url(f'{chat_room.pk}/'))
    assert get_chat_room.json()['detail'] == 'Not found.'
