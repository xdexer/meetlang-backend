import pytest
from model_bakery.baker import make_recipe


def make_url_summary(val: str) -> str:
    return f'/backend/chat/chatroom/{val}/summary/'


def make_url_all_summary() -> str:
    return f'/backend/chat/chatroom/all_summary/'


@pytest.mark.django_db
def test_get_chatroom_summary(api_client):
    user_1 = make_recipe('auth_module.user')
    user_2 = make_recipe('auth_module.user')
    chat_room = make_recipe('chat_management.chat_room', name='TestRoom', user_1=user_1, user_2=user_2)
    make_recipe('chat_management.chat_message', user=user_1, message="wiadro", chat=chat_room)
    make_recipe('user_management.user_details', user=user_1, first_name="This User")
    lan = make_recipe('user_management.language', **{'name': 'English',
                      'code': 'EN', 'common': True, 'flag': '\U0001F1EC\U0001F1E7'})
    lan2 = make_recipe('user_management.language', **{'name': 'Polish',
                                                      'code': 'PL', 'common': True, 'flag': '\U0001F1F5\U0001F1F1'})
    make_recipe('user_management.user_details', user=user_2,
                first_name="Other User", native_language=lan, language_to_learn=lan2)

    api_client.force_authenticate(user_1)
    connect_attempt = api_client.get(make_url_summary(f'{chat_room.pk}')).json()

    assert connect_attempt['other_user_name'] == "Other User"
    assert connect_attempt['last_message'] == "wiadro"
    assert connect_attempt['chat_id'] == chat_room.pk
    assert connect_attempt['avatar'] is None
    assert connect_attempt['native_flag'] == lan.flag
    assert connect_attempt['learning_flag'] == lan2.flag


@pytest.mark.django_db
def test_get_chatroom_summary_empty_last_message(api_client):
    user_1 = make_recipe('auth_module.user')
    user_2 = make_recipe('auth_module.user')
    chat_room = make_recipe('chat_management.chat_room', name='TestRoom', user_1=user_1, user_2=user_2)
    make_recipe('user_management.user_details', user=user_1, first_name="This User")
    make_recipe('user_management.user_details', user=user_2, first_name="Other User")

    api_client.force_authenticate(user_1)
    connect_attempt = api_client.get(make_url_summary(f'{chat_room.pk}')).json()

    assert connect_attempt['other_user_name'] == "Other User"
    assert connect_attempt['last_message'] == ""
    assert connect_attempt['chat_id'] == chat_room.pk


@pytest.mark.django_db
def test_get_chatroom_all_summary(api_client):
    user_1 = make_recipe('auth_module.user')
    user_2 = make_recipe('auth_module.user')
    user_3 = make_recipe('auth_module.user')
    user_4 = make_recipe('auth_module.user')

    chat_room_1 = make_recipe('chat_management.chat_room', name='TestRoom1', user_1=user_1, user_2=user_2)
    chat_room_2 = make_recipe('chat_management.chat_room', name='TestRoom2', user_1=user_1, user_2=user_4)
    chat_room_3 = make_recipe('chat_management.chat_room', name='TestRoom3', user_1=user_3, user_2=user_4)

    for i in range(10):
        make_recipe('chat_management.chat_message', user=user_1, message=f"wiadro{i}", chat=chat_room_1)

    make_recipe('chat_management.chat_message', user=user_1, message="wiadro3", chat=chat_room_2)
    make_recipe('chat_management.chat_message', user=user_1, message="wiadro4", chat=chat_room_3)

    make_recipe('user_management.user_details', user=user_1, first_name="This User")
    make_recipe('user_management.user_details', user=user_2, first_name="Other User")
    make_recipe('user_management.user_details', user=user_3, first_name="Not connected user")
    make_recipe('user_management.user_details', user=user_4, first_name="Another User")

    api_client.force_authenticate(user_1)
    connect_attempt = api_client.get(make_url_all_summary()).json()

    assert len(connect_attempt) == 2
    print(connect_attempt)
    assert connect_attempt[0]['last_message'] == 'wiadro9'
    assert connect_attempt[0]['other_user_name'] == 'Other User'
    assert connect_attempt[0]['chat_id'] == chat_room_1.pk

    assert connect_attempt[1]['last_message'] == 'wiadro3'
    assert connect_attempt[1]['other_user_name'] == 'Another User'
    assert connect_attempt[1]['chat_id'] == chat_room_2.pk
