import pytest
from model_bakery.baker import make_recipe

from auth_module.models.meetlang_user import MeetlangUser
from chat_management.models.chat_models import ChatMessage


def connect_url() -> str:
    return '/backend/chat/centrifugo/connect/'


def subscribe_url() -> str:
    return '/backend/chat/centrifugo/subscribe/'


def publish_url() -> str:
    return '/backend/chat/centrifugo/publish/'


@pytest.mark.django_db
def test_connect_authenticated_user(api_client):
    user_1: MeetlangUser = MeetlangUser.objects.create(email='testowy0@gmail.com',)
    user_1.set_password('test0')
    user_1.save()
    user_2 = make_recipe('auth_module.user')
    make_recipe('chat_management.chat_room', name='TestRoom', user_1=user_1, user_2=user_2)

    api_client.force_authenticate(user_1)
    connect_attempt = api_client.post(connect_url(), {
        "client": "9336a229-2400-4ebc-8c50-0a643d22e8a0",
        "transport": "websocket",
        "protocol": "json",
        "encoding": "json"
    })
    assert connect_attempt.json()['result']['user'] == str(user_1.pk)
    assert connect_attempt.json()['result']['data']['user'] == str(user_1.pk)


@pytest.mark.django_db
def test_connect_not_authenticated_user(api_client):
    user_1 = make_recipe('auth_module.user')
    user_2 = make_recipe('auth_module.user')
    make_recipe('chat_management.chat_room', name='TestRoom', user_1=user_1, user_2=user_2)

    connect_attempt = api_client.post(connect_url(), {
        "client": "9336a229-2400-4ebc-8c50-0a643d22e8a0",
        "transport": "websocket",
        "protocol": "json",
        "encoding": "json"
    })

    assert connect_attempt.json()['detail'] == 'Authentication credentials were not provided.'


@pytest.mark.django_db
def test_subscribe_authenticated_user(api_client):
    user_1 = make_recipe('auth_module.user')
    user_2 = make_recipe('auth_module.user')
    chat_room = make_recipe('chat_management.chat_room', name='testchat1', user_1=user_1, user_2=user_2)

    api_client.force_authenticate(user_1)
    subscribe_attempt = api_client.post(subscribe_url(), {
        "client": "9336a229-2400-4ebc-8c50-0a643d22e8a0",
        "transport": "websocket",
        "protocol": "json",
        "encoding": "json",
        "user": f"{user_1.pk}",
        "channel": f"{chat_room.pk}"
    })

    assert subscribe_attempt.json()['result'] == {}


@pytest.mark.django_db
def test_subscribe_user_not_in_chatroom(api_client):
    user_1 = make_recipe('auth_module.user')
    user_2 = make_recipe('auth_module.user')
    chat_room = make_recipe('chat_management.chat_room', name='testchat1', user_1=user_1, user_2=user_2)

    user_3 = make_recipe('auth_module.user')
    api_client.force_authenticate(user_3)
    subscribe_attempt = api_client.post(subscribe_url(), {
        "client": "9336a229-2400-4ebc-8c50-0a643d22e8a0",
        "transport": "websocket",
        "protocol": "json",
        "encoding": "json",
        "user": f"{user_3.pk}",
        "channel": f"{chat_room.pk}"
    })

    assert subscribe_attempt.json()['error'] == '1400'


@pytest.mark.django_db
def test_subscribe_chatroom_does_not_exist(api_client):
    user_1 = make_recipe('auth_module.user')
    user_2 = make_recipe('auth_module.user')
    chat_room = make_recipe('chat_management.chat_room', name='testchat1', user_1=user_1, user_2=user_2)

    api_client.force_authenticate(user_1)
    subscribe_attempt = api_client.post(subscribe_url(), {
        "client": "9336a229-2400-4ebc-8c50-0a643d22e8a0",
        "transport": "websocket",
        "protocol": "json",
        "encoding": "json",
        "user": f"{user_1.pk}",
        "channel": f"{user_1.pk+1}"
    })

    assert subscribe_attempt.json()['error'] == '1500'


@pytest.mark.django_db
def test_publish_save_message(api_client):
    user_1 = make_recipe('auth_module.user')
    user_2 = make_recipe('auth_module.user')
    chat_room = make_recipe('chat_management.chat_room', name='testchat1', user_1=user_1, user_2=user_2)
    api_client.force_authenticate(user_1)
    assert ChatMessage.objects.all().count() == 0
    publish_attempt = api_client.post(publish_url(), {
        "client": "9336a229-2400-4ebc-8c50-0a643d22e8a0",
        "transport": "websocket",
        "protocol": "json",
        "encoding": "json",
        "user": f"{user_1.pk}",
        "channel": f"{chat_room.pk}",
        "data": {"input": "hello"}
    }, format="json")
    assert ChatMessage.objects.all().count() == 1
    assert publish_attempt.json()['result']['data']['user'] == f"{user_1.pk}"
    assert publish_attempt.json()['result']['data']['message'] == 'hello'
