import pytest
from model_bakery.baker import make_recipe

from auth_module.models.meetlang_user import MeetlangUser
from chat_management.models.chat_models import ChatMessage, ChatRoom
from django.utils import timezone


def make_url(val: str) -> str:
    return f'/backend/chat/message_history/?room={val}'


@pytest.mark.django_db
def test_get_messages(api_client):
    user_1 = make_recipe('auth_module.user')
    user_2 = make_recipe('auth_module.user')
    chat_room = make_recipe('chat_management.chat_room', name='TestRoom', user_1=user_1, user_2=user_2)
    for i in range(50):
        make_recipe('chat_management.chat_message', user=user_1,
                    message=f"wiadro{i}", chat=chat_room, timestamp=timezone.now())
    for i in range(50, 100):
        make_recipe('chat_management.chat_message', user=user_2,
                    message=f"wiaderko{i}", chat=chat_room, timestamp=timezone.now())
    make_recipe('user_management.user_details', user=user_1, first_name="This User")
    make_recipe('user_management.user_details', user=user_2, first_name="Other User")

    api_client.force_authenticate(user_1)
    connect_attempt = api_client.get(make_url(f'{chat_room.id}')).data

    assert connect_attempt['count'] == 100
    assert connect_attempt['results'][0]['id'] == 100
    assert connect_attempt['results'][0]['message'] == 'wiaderko99'
    assert connect_attempt['previous'] is None
    assert connect_attempt['next'] is not None


@pytest.mark.django_db
def test_get_messages_when_no_messages(api_client):
    user_1 = make_recipe('auth_module.user')
    user_2 = make_recipe('auth_module.user')
    chat_room = make_recipe('chat_management.chat_room', name='TestRoom', user_1=user_1, user_2=user_2)
    make_recipe('user_management.user_details', user=user_1, first_name="This User")
    make_recipe('user_management.user_details', user=user_2, first_name="Other User")

    api_client.force_authenticate(user_1)
    connect_attempt = api_client.get(make_url(f'{chat_room.id}')).data

    assert connect_attempt['count'] == 0
    assert connect_attempt['previous'] is None
    assert connect_attempt['next'] is None


@pytest.mark.django_db
def test_get_messages_pagination(api_client):
    user_1 = make_recipe('auth_module.user')
    user_2 = make_recipe('auth_module.user')
    chat_room = make_recipe('chat_management.chat_room', name='TestRoom', user_1=user_1, user_2=user_2)
    for i in range(50):
        make_recipe('chat_management.chat_message', user=user_1,
                    message=f"wiadro{i}", chat=chat_room, timestamp=timezone.now())
    for i in range(50, 100):
        make_recipe('chat_management.chat_message', user=user_2,
                    message=f"wiaderko{i}", chat=chat_room, timestamp=timezone.now())
    make_recipe('user_management.user_details', user=user_1, first_name="This User")
    make_recipe('user_management.user_details', user=user_2, first_name="Other User")

    api_client.force_authenticate(user_1)
    connect_attempt = api_client.get(make_url(f'{chat_room.id}&page=2')).data

    assert connect_attempt['count'] == 100
    assert connect_attempt['results'][0]['id'] == 50
    assert connect_attempt['results'][0]['message'] == 'wiadro49'
    assert connect_attempt['previous'] is not None
    assert connect_attempt['next'] is None
