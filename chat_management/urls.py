"""meetlang_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from rest_framework.routers import DefaultRouter
from django.urls import path

from chat_management.views.centrifugo_viewset import CentrifugoViewSet
from chat_management.views.chatroom_viewset import ChatRoomViewSet
from chat_management.views.chatmessage_listapiview import ChatMessageListAPIView

router = DefaultRouter()
router.register(r'chatroom', ChatRoomViewSet, basename='chatroom')
router.register(r'centrifugo', CentrifugoViewSet, basename='centrifugo')

urlpatterns = [
    path('message_history/', ChatMessageListAPIView.as_view(), name='message_history'),
]

urlpatterns += router.urls
