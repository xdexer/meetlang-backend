from django.utils import timezone

from model_bakery.recipe import Recipe, foreign_key, seq

from auth_module.baker_recipes import user
from chat_management.models.chat_models import ChatMessage, ChatRoom

chat_room = Recipe(
    ChatRoom,
    name=seq('Name '),
    user_1=foreign_key(user),
    user_2=foreign_key(user)
)

chat_message = Recipe(
    ChatMessage,
    user=foreign_key(user),
    message=seq('Message '),
    chat=foreign_key(chat_room),
    timestamp=timezone.now()
)
