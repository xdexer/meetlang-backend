from typing import List

from django.db.models import Q
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from auth_module.models.meetlang_user import MeetlangUser
from chat_management.models.chat_models import ChatMessage, ChatRoom
from chat_management.serializers.chatroom_serializer import ChatRoomSerializer
from user_management.models.user_details import UserDetails


class ChatRoomViewSet(viewsets.ModelViewSet):
    serializer_class = ChatRoomSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        queryset = ChatRoom.objects.filter(Q(user_1=user) | Q(user_2=user))
        return queryset

    def get_summary(self, chat_room, request):
        try:
            last_message: ChatMessage = ChatMessage.objects.filter(chat=chat_room).latest('id')
        except ChatMessage.DoesNotExist:
            last_message = ChatMessage(message='')

        other_user: MeetlangUser = chat_room.user_2 if chat_room.user_1 == request.user else chat_room.user_1
        other_user_details: UserDetails = UserDetails.objects.get(user=other_user)

        res = {
            'other_user_id': other_user.pk,
            'other_user_name': other_user_details.first_name,
            'avatar': other_user_details.avatar.url if other_user_details.avatar else None,
            'last_message': last_message.message,
            'chat_id': chat_room.pk,
            'native_flag': other_user_details.native_language.flag,
            'learning_flag': other_user_details.language_to_learn.flag,
            'is_online': other_user_details.is_active
        }

        return res

    @action(methods=['get'], detail=True)
    def summary(self, request, pk=True, *args, **kwargs):
        return Response(self.get_summary(self.get_object(), request))

    @action(methods=['get'], detail=False)
    def all_summary(self, request, *args, **kwargs):
        return Response([self.get_summary(chat_room, request) for chat_room in list(self.get_queryset())])
