from django.db.models import Q
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination
from chat_management.models.chat_models import ChatMessage, ChatRoom
from chat_management.serializers.chatmessage_serializer import ChatMessageSerializer


class ChatMessagePagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 100


class ChatMessageListAPIView(ListAPIView):
    pagination_class = ChatMessagePagination
    serializer_class = ChatMessageSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        room_id = self.request.GET.get('room')
        room = ChatRoom.objects.filter(Q(user_1=user) | Q(user_2=user)).get(id=room_id)
        queryset = ChatMessage.objects.filter(chat=room).order_by('-timestamp')
        return queryset
