from django.utils import timezone

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated

from chat_management.models.chat_models import ChatMessage, ChatRoom


class UserNotInChatRoom(Exception):
    ...


class CentrifugoViewSet(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]

    @csrf_exempt
    @action(detail=False, methods=['post'])
    def connect(self, request):
        response = {
            'result': {
                'user': f'{request.user.pk}',
                'data': {
                    'user': f'{request.user.pk}'
                }
            }
        }
        return JsonResponse(response)

    @csrf_exempt
    @action(detail=False, methods=['post'])
    def publish(self, request):
        body_data = request.data
        try:
            message: ChatMessage = ChatMessage.objects.create(
                user=request.user,
                message=body_data['data']['input'],
                chat=ChatRoom.objects.get(pk=body_data['channel']),
                timestamp=timezone.now()
            )
        except ChatRoom.DoesNotExist:
            return JsonResponse({
                'result': {
                    'data': {
                        'status': '404'
                    }
                }
            })


        response = {
            'result': {
                'data': {
                    'status': '200',
                    'user': f'{message.user.pk}',
                    'message': f'{message.message}',
                    'timestamp': f'{message.timestamp}'
                }
            }
        }
        return JsonResponse(response)

    @csrf_exempt
    @action(detail=False, methods=['post'])
    def subscribe(self, request):
        body_data = request.data
        try:
            room_to_connect = ChatRoom.objects.get(pk=body_data['channel'])
            user_to_connect = int(body_data['user'])
            if user_to_connect in (room_to_connect.user_1.pk, room_to_connect.user_2.pk):
                response = {
                    'result': {}
                }
            else:
                raise UserNotInChatRoom("User is not in the ChatRoom")
        except UserNotInChatRoom as error_message:
            response = {
                'error': '1400',
                'message': str(error_message)
            }
        except ChatRoom.DoesNotExist:
            response = {
                'error': '1500',
                'message': 'This chat room does not exist'
            }

        return JsonResponse(response)
