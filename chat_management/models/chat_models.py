from django.db import models
from django.utils import timezone
from auth_module.models.meetlang_user import MeetlangUser


class ChatRoom(models.Model):
    name = models.CharField(max_length=40)
    user_1 = models.ForeignKey(MeetlangUser, on_delete=models.CASCADE, related_name='user_1')
    user_2 = models.ForeignKey(MeetlangUser, on_delete=models.CASCADE, related_name='user_2')


class ChatMessage(models.Model):
    user = models.ForeignKey(MeetlangUser, on_delete=models.CASCADE)
    message = models.TextField(null=False)
    chat = models.ForeignKey(ChatRoom, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(default=timezone.now)
